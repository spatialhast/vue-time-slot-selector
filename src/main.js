import Vue from 'vue'
import Vuetify from 'vuetify'
import 'vuetify/src/stylus/app.styl'

import App from './App.vue'

Vue.use(Vuetify, {
  iconfont: 'md',
})

Vue.config.productionTip = false

new Vue({
  render: h => h(App),
}).$mount('#app')